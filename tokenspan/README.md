# iamtokenizing.tokentokens

The core module and classes.

## `Span`

Deals with the cutting a string in terms of collection of `range` objects from a parent string. Also deals with basic arithmetic of string (seen as a set of character positions from the parent string): union (+ operation), difference (- operation), intersection (* operation) and symmetric_difference (/ operation).

## `Token` and `Tokens`

Based on the `Span` class, these two classes allow to deal with user-designed attributes on the flow of the token usage.

## tools.py

This module contains the tools to identify the different `Span`, `Token` and `Tokens` objects, and to give transversal methods they share.
