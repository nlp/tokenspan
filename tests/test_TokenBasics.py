"""
Test of the Token class

Run as 

python3 -m unittest -v tests/test_TokenBasics.py
"""

import unittest as ut

from tokenspan import Token

# # for construction
# class Empty(): pass
# self = Empty()

class TestTokenBasics(ut.TestCase):
    
    def setUp(self):
        self.ranges = [range(17,21),range(22,23),range(53,64)]
        root = '123456789'
        self.text = ''.join([str(i)+root for i in range(10)])
        self.extracted_text = '_'.join(self.text[r.start:r.stop] for r in self.ranges)
        self.token = Token(string=self.text,
                           ranges=self.ranges,
                           subtoksep='_')
        return None
    
    def test_exist(self,):
        """Existence of the normal attributes, and initializations checks."""
        token = Token(ranges=self.ranges,
                      string=self.text,)
        self.assertTrue(hasattr(token,'ranges'))
        self.assertTrue(getattr(token,'ranges')==self.ranges)
        self.assertTrue(hasattr(token,'string'))
        self.assertTrue(getattr(token,'string')==self.text)
        self.assertTrue(getattr(token,'subtoksep')==' ')
        token = Token(string=self.text,
                      subtoksep='_&_')
        self.assertTrue(hasattr(token,'ranges'))
        self.assertTrue(getattr(token,'ranges')==[range(0,len(self.text)),])
        self.assertTrue(hasattr(token,'string'))
        self.assertTrue(getattr(token,'string')==self.text)
        self.assertTrue(getattr(token,'subtoksep')=='_&_')
        token = Token(ranges=self.ranges,
                      string=self.text)
        self.assertTrue(hasattr(token,'ranges'))
        self.assertTrue(getattr(token,'ranges')==self.ranges)
        self.assertTrue(hasattr(token,'string'))
        self.assertTrue(getattr(token,'string')==self.text)
        with self.assertRaises(ValueError):
            token = Token(ranges=(1,2))
        return None

    def test_setattrANDcopy(self,):
        """Set some extra attributes, and pass them to copies."""
        token = Token(string=self.text,)
        token.setattr('test1',a=1,b=2)
        self.assertTrue(hasattr(token,'test1'))
        self.assertTrue(getattr(token,'test1')==dict(a=1,b=2))
        tok1 = token.copy()
        self.assertTrue(hasattr(tok1,'test1'))
        self.assertTrue(getattr(tok1,'test1')==dict(a=1,b=2))
        token.test1.update(b=0,c=3)
        self.assertTrue(getattr(token,'test1')==dict(a=1,b=0,c=3))
        self.assertTrue(getattr(tok1,'test1')==dict(a=1,b=2))
        with self.assertRaises(AttributeError):
            token.setattr('test1')
        token.setattr('test2',dict(a=0,b=1))
        token.setattr('test3',dict(a=0,b=1),c=2,d=3)
        self.assertTrue(token.test2 == dict(a=0,b=1))
        self.assertTrue(token.test3 == dict(a=0,b=1,c=2,d=3))
        tok2 = token.copy()
        self.assertTrue(tok2.test2 == dict(a=0,b=1))
        self.assertTrue(tok2.test3 == dict(a=0,b=1,c=2,d=3))
        tok2.test3 = dict(a=1,b=2,c=3)
        self.assertTrue(tok2.test3 == dict(a=1,b=2,c=3))
        self.assertTrue(token.test3 == dict(a=0,b=1,c=2,d=3))
        self.assertTrue(token._extra_attributes == tok2._extra_attributes)
        tok2 = token.copy(reset_attributes=True)
        self.assertTrue(tok2.attributes==frozenset())
        return None
    
    def test_removeANDappendSimple(self,):
        """Remove a range and append a range to a Token"""
        tok = Token(string=self.text)
        self.assertTrue(tok.ranges == [range(0,len(tok)),])
        tok.remove_range(range(12,56))
        s = self.text[:12]+' '+self.text[56:]
        self.assertTrue(len(tok.ranges)==2)
        self.assertTrue(str(tok) == s)
        tok.remove_range(range(13,45))
        self.assertTrue(str(tok) == s)
        self.assertTrue(len(tok.ranges)==2)
        tok.append_range(range(150,185))
        self.assertTrue(len(tok.ranges)==2)
        self.assertTrue(str(tok) == s)
        tok.append_range(range(12,56))
        self.assertTrue(len(tok.ranges)==1)
        self.assertTrue(str(tok) == self.text)
        tok.append_range(range(12,56))
        self.assertTrue(len(tok.ranges)==1)
        self.assertTrue(str(tok) == self.text)
        tok.remove_range(range(-12,25))
        self.assertTrue(len(tok.ranges)==1)
        self.assertTrue(str(tok) == self.text[25:])
        tok.remove_range(range(-12,25))
        self.assertTrue(len(tok.ranges)==1)
        self.assertTrue(str(tok) == self.text[25:])
        return None

    def test_removeANDappendDouble(self,):
        """Remove a range and append a range to a Token"""
        tok = Token(string=self.text,ranges=[range(10,20),range(50,70)])
        self.assertTrue(len(tok) == 31)
        tok.remove_range(range(12,56))
        s = self.text[10:12]+' '+self.text[56:70]
        self.assertTrue(len(tok.ranges)==2)
        self.assertTrue(str(tok) == s)
        tok.remove_range(range(13,45))
        self.assertTrue(len(tok.ranges)==2)
        self.assertTrue(str(tok) == s)
        self.assertTrue(tok == tok.remove_range(range(13,45)))
        tok.append_range(range(150,185))
        self.assertTrue(len(tok.ranges)==2)
        self.assertTrue(str(tok) == s)
        tok.append_range(range(12,56))
        self.assertTrue(len(tok.ranges)==1)
        self.assertTrue(str(tok) == self.text[10:70])
        tok.append_range(range(12,56))
        self.assertTrue(len(tok.ranges)==1)
        self.assertTrue(str(tok) == self.text[10:70])
        tok.remove_range(range(-12,25))
        self.assertTrue(len(tok.ranges)==1)
        self.assertTrue(str(tok) == self.text[25:70])
        tok.remove_range(range(-12,25))
        self.assertTrue(len(tok.ranges)==1)
        self.assertTrue(str(tok) == self.text[25:70])
        return None

    def test_equivalence(self,):
        """Equivalence of two instances"""
        tok1 = Token(string=self.text)
        tok2 = Token(string=self.text)
        tok3 = Token(string=self.text)
        tok4 = Token(string=self.text)        
        self.assertTrue(tok1==tok2)
        tok4.remove_range(range(12,56))
        self.assertTrue(tok4!=tok3)
        tok4.append_range(range(12,56))
        self.assertTrue(tok4==tok3)
        tok1.setattr('test',a=0,b=1)
        tok2.setattr('test',a=0,b=1)
        self.assertTrue(tok1==tok2)
        tok2.test.update(c=2)
        self.assertFalse(tok1==tok2)
        tok3 = tok1.copy(reset_attributes=True)
        tok3.carry_attributes = False
        tok3.setattr('test',a=1,b=3)
        self.assertTrue(all([tok1.carry_attributes,tok2.carry_attributes]))
        self.assertFalse(all([tok3.carry_attributes,tok2.carry_attributes]))
        self.assertFalse(tok1==tok2)
        self.assertTrue(tok3==tok2)
        return None    
    
    def test_getitems(self,):
        """Verify the __getitem__ magic function, and __str__ and __len__ and __bool__ as well."""
        string = '7892_2_34567896123'
        self.assertTrue(string[:15]==self.token[:15])
        self.assertTrue(len(self.token)==len(string))
        self.assertTrue(self.token[0:0]==str())
        self.assertTrue(self.token[:len(self.token)]==str(self.token))
        self.assertTrue(str(self.token)==string)
        self.assertTrue(bool(self.token)==bool(string))
        token = Token()
        self.assertFalse(bool(token))
        token.string = 'test'
        self.assertFalse(bool(token))
        token.ranges = [range(12,24),]
        self.assertTrue(bool(token))
        return None
    
    def test_stringMethods(self,):
        """Verify the string methods are correct"""
        string = self.token[:]
        string_methods = ['upper','lower','swapcase','capitalize','casefold',
                          'isalnum','isalpha','isascii','isdecimal','isdigit',
                          'isidentifier','islower','isnumeric','isprintable',
                          'isspace','istitle','isupper']
        method_dict = dict()
        for meth in string_methods:
            get1 = getattr(self.token,meth)()
            get2 = getattr(string,meth)()
            self.assertTrue(get1==get2)
            method_dict[meth] = get2
        self.token.set_string_methods()
        self.assertTrue(hasattr(self.token,'string_methods'))
        self.assertTrue(self.token.string_methods==method_dict)
        string_methods = ['startswith','endswith']
        for meth in string_methods:
            get1 = getattr(self.token,meth)('test')
            get2 = getattr(string,meth)('test')
            self.assertTrue(get1==get2)
        return None
    
    def test_contains(self,):
        """Token1 in Token2 verification"""
        text = 'I am a string used for a test with two different _a_ symbol'
        t1 = Token(text)
        t2 = Token(text,ranges=[range(5,6),])
        t3 = Token(text,ranges=[range(23,24),])
        t4 = Token('a')
        
        self.assertTrue(t2 in t1)
        self.assertFalse(t1 in t2)
        self.assertTrue(t3 in t1)
        self.assertFalse(t1 in t3)
        self.assertFalse(t2 in t3)
        self.assertTrue(str(t2) in t3)
        self.assertTrue(str(t3) in t2)
        self.assertFalse(t4 in t2)
        self.assertFalse(t4 in t3)
        self.assertFalse(t2 in t4)
        self.assertFalse(t3 in t4)
        self.assertTrue(str(t4) in t2)
        self.assertTrue(str(t4) in t3)
        return None
    
    def test_basicOperations(self,):
        """+ - * / verification"""
        text = 'I am a string following a conversation'
        t1 = Token(text)
        t2 = Token(text,ranges=[range(5,6),])
        t3 = Token(text,ranges=[range(24,25),])
        t4 = Token('a')
        
        t = t2+t3
        self.assertTrue(str(t)=='a a')
        self.assertTrue(t.ranges==[range(5, 6), range(24, 25)])
        self.assertTrue(t2.ranges==[range(5,6),])
        self.assertTrue(t3.ranges==[range(24,25),])
        
        t = t2-t3
        self.assertTrue(str(t)=='a')
        self.assertTrue(t==t2)
        self.assertTrue(t.ranges==[range(5,6),])
        self.assertTrue(t2.ranges==[range(5,6),])
        self.assertTrue(t3.ranges==[range(24,25),])
        
        t = t2-t1
        self.assertTrue(str(t)=='')
        self.assertFalse(bool(t))
        self.assertTrue(t.ranges==[])
        self.assertTrue(t2.ranges==[range(5,6),])
        self.assertTrue(t1.ranges==[range(len(text))])
        
        t = t1-t2
        self.assertTrue(str(t)=='I am   string following a conversation')
        self.assertTrue(t.ranges==[range(0, 5), range(6, 38)])
        self.assertTrue(t2.ranges==[range(5,6),])
        self.assertTrue(t1.ranges==[range(len(text))])
        
        with self.assertRaises(TypeError):
            t1-t4
        with self.assertRaises(TypeError):
            t1+t4
        with self.assertRaises(TypeError):
            t1*t4
        with self.assertRaises(TypeError):
            t1/t4
        
        t = t2/t3
        self.assertTrue(str(t)=='a a')
        self.assertTrue(t.ranges==[range(5, 6), range(24, 25)])
        self.assertTrue(t2.ranges==[range(5,6),])
        self.assertTrue(t3.ranges==[range(24,25),])
        
        t = t3/t2
        self.assertTrue(str(t)=='a a')
        self.assertTrue(t.ranges==[range(5, 6), range(24, 25)])
        self.assertTrue(t2.ranges==[range(5,6),])
        self.assertTrue(t3.ranges==[range(24,25),])
        
        t = t2*t3
        self.assertTrue(str(t)=='')
        self.assertFalse(bool(t))
        self.assertTrue(t.ranges==[])
        self.assertTrue(t2.ranges==[range(5,6),])
        self.assertTrue(t3.ranges==[range(24,25),])
        
        t2 = Token(text,ranges=[range(5,10),range(15,20)])
        t3 = Token(text,ranges=[range(8,12),])
        
        t = t2/t3
        self.assertTrue(str(t)=='a s in ollow')
        self.assertTrue(t.ranges==[range(5, 8), range(10, 12), range(15, 20)])
        self.assertTrue(t2.ranges==[range(5,10),range(15,20)])
        self.assertTrue(t3.ranges==[range(8,12),])
        
        t = t3/t2
        self.assertTrue(str(t)=='a s in ollow')
        self.assertTrue(t.ranges==[range(5, 8), range(10, 12), range(15, 20)])
        self.assertTrue(t2.ranges==[range(5,10),range(15,20)])
        self.assertTrue(t3.ranges==[range(8,12),])
        
        t = t2*t3
        self.assertTrue(str(t)=='tr')
        self.assertTrue(t.ranges==[range(8,10)])
        self.assertTrue(t2.ranges==[range(5,10),range(15,20)])
        self.assertTrue(t3.ranges==[range(8,12),])
        
        t = t2-t1
        self.assertFalse(bool(t))
        
        t = t1-t2
        self.assertTrue(bool(t))
        self.assertTrue(str(t)=='I am  ing f ing a conversation')
        self.assertTrue(t.ranges==[range(0, 5), range(10, 15), range(20, 38)])
        return None
    
    # def test_lowerthan(self,):
    #     """The < and <= operations"""
    #     tok1 = Token(string=self.text,ranges=[range(10,20),])
    #     tok2 = Token(string=self.text,ranges=[range(30),])
    #     self.assertTrue(str(tok1) in str(tok2))
    #     self.assertTrue(tok1.string == tok2.string)
    #     self.assertTrue(len(tok1.subtoksep) == len(tok2.subtoksep))
    #     self.assertTrue(set(tok1._extra_attributes) <= set(tok2._extra_attributes))
    #     self.assertTrue(all([tok1.carry_attributes,tok2.carry_attributes]))
    #     self.assertTrue(tok1 < tok2)
    #     self.assertFalse(tok2 < tok1)
    #     tok1 = Token(string=self.text,ranges=[range(10,20),range(40,50)])
    #     tok2 = Token(string=self.text,ranges=[range(30),])
    #     self.assertFalse(str(tok1) in str(tok2))
    #     self.assertTrue(tok1.string == tok2.string)
    #     self.assertTrue(len(tok1.subtoksep) == len(tok2.subtoksep))
    #     self.assertTrue(set(tok1._extra_attributes) <= set(tok2._extra_attributes))
    #     self.assertTrue(all([tok1.carry_attributes,tok2.carry_attributes]))
    #     self.assertFalse(tok1 < tok2)
    #     self.assertFalse(tok2 < tok1)
    #     tok1 = Token(string=self.text,ranges=[range(10,20),range(40,50)])
    #     tok2 = Token(string=self.text,ranges=[range(10,30),range(40,51)])
    #     self.assertFalse(str(tok1) in str(tok2))
    #     check = all(any(r1.start>=r2.start and r1.stop<=r2.stop
    #                      for r2 in tok2.ranges)
    #                 for r1 in tok1.ranges)
    #     self.assertTrue(check)
    #     self.assertTrue(tok1.string == tok2.string)
    #     self.assertTrue(len(tok1.subtoksep) == len(tok2.subtoksep))
    #     self.assertTrue(set(tok1._extra_attributes) <= set(tok2._extra_attributes))
    #     self.assertTrue(all([tok1.carry_attributes,tok2.carry_attributes]))
    #     self.assertTrue(tok1 < tok2)
    #     check = all(any(r1.start>=r2.start and r1.stop<=r2.stop
    #                      for r2 in tok1.ranges)
    #                 for r1 in tok2.ranges)
    #     self.assertFalse(check)
    #     self.assertFalse(tok2 < tok1)
    #     return None
    
    def test_add(self,):
        tok1 = Token(string=self.text,ranges=[range(10,20),])
        tok2 = Token(string=self.text,ranges=[range(30,40),])
        tok3 = tok1 + tok2
        s = self.text[10:20]+' '+self.text[30:40]
        self.assertTrue(s == str(tok3))
        tok1 += tok2
        self.assertTrue(s == str(tok1))
        self.assertTrue(tok1==tok3)
        return None
    
    def test_fusionAttributes(self,):
        """Check for fusion_attributes, but also the dictionnary behavior of Token"""
        tok1 = Token(string=self.text,ranges=[range(10,20),])
        tok1.setattr('test1',dict(a=1,b=2))
        tok2 = Token(string=self.text,ranges=[range(30,40),])
        tok2.setattr('test2',dict(c=3,d=4))
        tok3 = tok1.fusion_attributes(tok2)
        # self.assertTrue(tok3.attributes == frozenset({'test1', 'test2'}))
        # list(tok3.items()) == [('test1', {'a': 1, 'b': 2}), ('test2', {'c': 3, 'd': 4})]
        # tok4 = tok2.fusion_attributes(tok1)
        # tok3 == tok4
        # tok3.ranges == tok4.ranges
        # self.assertTrue(tok3 <= tok4)
        # self.assertTrue(tok4 <= tok3)
        # self.assertTrue(tok3.carry_attributes == tok4.carry_attributes)
        # self.assertTrue(tok3.attributes == tok4.attributes)
        # self.assertTrue(tok3.test1 == tok4.test1)
        # self.assertTrue(tok3.test2 == tok4.test2)
        # tok2.setattr('test1',dict(a=2,b=2,c=3))
        # tok3 = tok1.fusion_attributes(tok2)
        # self.assertTrue(tok3.attributes==frozenset({'test1_fusion1', 'test1_fusion2', 'test2'}))
        # self.assertTrue(tok3.test1_fusion1=={'a': 1, 'b': 2, 'c': 3})
        # self.assertTrue(tok3.test1_fusion2=={'a': 2, 'b': 2, 'c': 3})
        # self.assertTrue(tok3.test2==dict(c=3,d=4))
        # with self.assertRaises(AttributeError):
        #     tok3.test1
        # tok2 = tok2.copy(reset_attributes=True)
        # self.assertTrue(tok2.attributes==frozenset())
        # tok2.setattr('test1',dict(a=1,b=2))
        # tok2.setattr('test2',dict(c=3,d=4))
        # tok3 = tok1.fusion_attributes(tok2)
        # self.assertTrue(tok3.attributes==frozenset({'test1', 'test2'}))
        # self.assertTrue(tok3.test1=={'a': 1, 'b': 2})
        # self.assertTrue(tok3.test2==dict(c=3,d=4))
        return None

    def test_addWithAttributes(self,):
        tok1 = Token(string=self.text,ranges=[range(10,20),])
        tok1.setattr('test1',dict(a=1,b=2))
        tok2 = Token(string=self.text,ranges=[range(30,40),])
        tok2.setattr('test2',dict(c=3,d=4))
        tok2.setattr('test1',dict(e=5,c=3))
        tok3 = tok1 + tok2
        s = self.text[10:20]+' '+self.text[30:40]
        self.assertTrue(s == str(tok3))
        tok1 += tok2
        self.assertTrue(s == str(tok3))
        self.assertTrue(tok3.test1 == dict(a=[1,{}],b=[2,{}],
                                           c=[{},3],e=[{},5]))
        self.assertTrue(tok3.test2 == dict(c=[{},3],d=[{},4]))
        
        tok1 = Token(string=self.text,ranges=[range(10,20),])
        tok1.setattr('test')
        tok2 = Token(string=self.text,ranges=[range(30,40),],
                     carry_attributes=False)
        tok2.setattr('test')
        tok3 = tok1 + tok2
        with self.assertRaises(AttributeError):
            tok3.test
        
        tok1 = Token(string=self.text,ranges=[range(10,20),])
        tok1.setattr('test',dict(a=1,b=2))
        tok2 = Token(string=self.text,ranges=[range(30,40),])
        tok2.setattr('test',dict(c=3,d=4))
        tok3 = tok1 + tok2
        self.assertTrue(tok3.test == dict(a=[1,{}],b=[2,{}],
                                          c=[{},3],d=[{},4]))
        return None
        

if __name__ == '__main__':
    ut.main()
