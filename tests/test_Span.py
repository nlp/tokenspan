"""
Test of the span class

Run as 

python3 -m unittest -v test_Span.py

from the present folder
"""

import unittest as ut
import random as r

from tokenspan import Span

# # for construction
# class Empty(): pass
# self = Empty()

class TestSpanBasics(ut.TestCase):
    
    def setUp(self):
        self.subtoksep = '_'
        self.ranges = [range(17,21),range(22,23),range(53,64)]
        root = '123456789'
        self.text = ''.join([str(i)+root for i in range(10)])
        self.extracted_text = '_'.join(self.text[r.start:r.stop] for r in self.ranges)
        self.span = Span(string=self.text,
                         ranges=self.ranges,
                         subtoksep=self.subtoksep)
        return None
    
    def test_exist(self,):
        """Existence of the normal attributes, and initializations checks."""
        span = Span(ranges=self.ranges,
                    string=self.text,
                    subtoksep=self.subtoksep)
        self.assertTrue(hasattr(span,'ranges'))
        self.assertTrue(getattr(span,'ranges')==self.ranges)
        self.assertTrue(hasattr(span,'string'))
        self.assertTrue(getattr(span,'string')==self.text)
        self.assertTrue(hasattr(span,'subtoksep'))
        self.assertTrue(getattr(span,'subtoksep')==self.subtoksep)
        span = Span(ranges=None,
                    string=self.text,
                    subtoksep='_&_')
        self.assertTrue(hasattr(span,'ranges'))
        self.assertTrue(getattr(span,'ranges')==[range(0,len(self.text)),])
        self.assertTrue(hasattr(span,'string'))
        self.assertTrue(getattr(span,'string')==self.text)
        self.assertTrue(hasattr(span,'subtoksep'))
        self.assertTrue(getattr(span,'subtoksep')=='_&_')
        span = Span(ranges=self.ranges,
                    string=self.text)
        self.assertTrue(hasattr(span,'ranges'))
        self.assertTrue(getattr(span,'ranges')==self.ranges)
        self.assertTrue(hasattr(span,'string'))
        self.assertTrue(getattr(span,'string')==self.text)
        self.assertTrue(hasattr(span,'subtoksep'))
        self.assertTrue(getattr(span,'subtoksep')==' ')
        with self.assertRaises(ValueError):
            span = Span(ranges=(2,4))
        return None
    
    def test_removeANDappendSimple(self,):
        """Remove a range and append a range to a span"""
        tok = Span(string=self.text)
        self.assertTrue(tok.ranges == [range(0,len(tok)),])
        tok.remove_range(range(12,56))
        s = self.text[:12]+' '+self.text[56:]
        self.assertTrue(len(tok.ranges)==2)
        self.assertTrue(str(tok) == s)
        tok.remove_range(range(13,45))
        self.assertTrue(str(tok) == s)
        self.assertTrue(len(tok.ranges)==2)
        tok.append_range(range(150,185))
        self.assertTrue(len(tok.ranges)==2)
        self.assertTrue(str(tok) == s)
        tok.append_range(range(12,56))
        self.assertTrue(len(tok.ranges)==1)
        self.assertTrue(str(tok) == self.text)
        tok.append_range(range(12,56))
        self.assertTrue(len(tok.ranges)==1)
        self.assertTrue(str(tok) == self.text)
        tok.remove_range(range(-12,25))
        self.assertTrue(len(tok.ranges)==1)
        self.assertTrue(str(tok) == self.text[25:])
        tok.remove_range(range(-12,25))
        self.assertTrue(len(tok.ranges)==1)
        self.assertTrue(str(tok) == self.text[25:])
        return None

    def test_removeANDappendDouble(self,):
        """Remove a range and append a range to a span"""
        tok = Span(string=self.text,ranges=[range(10,20),range(50,70)])
        self.assertTrue(len(tok) == 31)
        tok.remove_range(range(12,56))
        s = self.text[10:12]+' '+self.text[56:70]
        self.assertTrue(len(tok.ranges)==2)
        self.assertTrue(str(tok) == s)
        tok.remove_range(range(13,45))
        self.assertTrue(len(tok.ranges)==2)
        self.assertTrue(str(tok) == s)
        self.assertTrue(tok == tok.remove_range(range(13,45)))
        tok.append_range(range(150,185))
        self.assertTrue(len(tok.ranges)==2)
        self.assertTrue(str(tok) == s)
        tok.append_range(range(12,56))
        self.assertTrue(len(tok.ranges)==1)
        self.assertTrue(str(tok) == self.text[10:70])
        tok.append_range(range(12,56))
        self.assertTrue(len(tok.ranges)==1)
        self.assertTrue(str(tok) == self.text[10:70])
        tok.remove_range(range(-12,25))
        self.assertTrue(len(tok.ranges)==1)
        self.assertTrue(str(tok) == self.text[25:70])
        tok.remove_range(range(-12,25))
        self.assertTrue(len(tok.ranges)==1)
        self.assertTrue(str(tok) == self.text[25:70])
        return None

    def test_equivalence(self,):
        """Equivalence of two instances, test the hashing as well"""
        tok1 = Span(string=self.text)
        tok2 = Span(string=self.text)
        tok3 = Span(string=self.text[10:])
        tok4 = Span(string=self.text,ranges=[range(10,len(self.text))])
        
        self.assertTrue(tok1==tok2)
        self.assertTrue(tok1.string==tok2.string)
        self.assertTrue(tok1.ranges==tok2.ranges)
        self.assertTrue(str(tok1)==str(tok2))
        self.assertTrue(tok1.subtoksep==tok2.subtoksep)
        self.assertTrue(hash(tok1)==hash(tok2))
        
        tok1.remove_range(range(12,56))
        self.assertTrue(tok1!=tok2)
        self.assertFalse(tok1==tok2)
        self.assertTrue(tok1.string==tok2.string)
        self.assertFalse(tok1.ranges==tok2.ranges)
        self.assertFalse(str(tok1)==str(tok2))
        self.assertTrue(tok1.subtoksep==tok2.subtoksep)
        self.assertFalse(hash(tok1)==hash(tok2))
        
        tok1.append_range(range(12,56))
        self.assertTrue(tok1==tok2)
        self.assertTrue(tok1.string==tok2.string)
        self.assertTrue(tok1.ranges==tok2.ranges)
        self.assertTrue(str(tok1)==str(tok2))
        self.assertTrue(tok1.subtoksep==tok2.subtoksep)
        self.assertTrue(hash(tok1)==hash(tok2))
        
        self.assertFalse(tok1==tok3)
        self.assertFalse(tok1.string==tok3.string)
        self.assertFalse(tok1.ranges==tok3.ranges)
        self.assertFalse(str(tok1)==str(tok3))
        self.assertTrue(tok1.subtoksep==tok2.subtoksep)
        self.assertFalse(hash(tok1)==hash(tok3))
        
        self.assertFalse(tok4==tok3)
        self.assertFalse(tok4.string==tok3.string)
        self.assertTrue(str(tok4)==str(tok3))
        self.assertFalse(tok4.ranges==tok3.ranges)
        self.assertTrue(tok4.subtoksep==tok3.subtoksep)
        self.assertFalse(hash(tok4)==hash(tok3))
        
        tok4.append_range(range(0,10))
        self.assertTrue(tok1==tok4)
        self.assertTrue(hash(tok1)==hash(tok4))
        tok4.subtoksep='_'
        self.assertFalse(tok1==tok4)
        self.assertFalse(hash(tok1)==hash(tok4))
        return None    


    def test_subSpans(self,):
        """Get the different subSpans"""
        for i in range(len(self.ranges)):
            si = Span(string=self.text,
                      subtoksep=self.subtoksep,
                      ranges=[self.ranges[i],])
            self.assertTrue(si==self.span.subspans[i])
        return None
    
    def test_getitems(self,):
        """Verify the __getitem__ magic function, and __str__ and __len__ and __bool__ as well."""
        string = '7892_2_34567896123'
        self.assertTrue(string[:15]==self.span[:15])
        self.assertTrue(len(self.span)==len(string))
        self.assertTrue(self.span[0:0]==str())
        self.assertTrue(self.span[:len(self.span)]==str(self.span))
        self.assertTrue(str(self.span)==string)
        self.assertTrue(bool(self.span)==bool(string))
        span = Span()
        self.assertFalse(bool(span))
        span.string = 'test'
        self.assertFalse(bool(span))
        span.ranges = [range(12,24),]
        self.assertTrue(bool(span))
        return None
    
    def test_stringMethods(self,):
         """Verify the string methods are correct, when applied to str(Span)"""
         string = self.span[:]
         string_methods = ['upper','lower','swapcase','capitalize','casefold',
                           'isalnum','isalpha','isascii','isdecimal','isdigit',
                           'isidentifier','islower','isnumeric','isprintable',
                           'isspace','istitle','isupper']
         for meth in string_methods:
             get1 = getattr(str(self.span),meth)()
             get2 = getattr(string,meth)()
             self.assertTrue(get1==get2)
         string_methods = ['startswith','endswith']
         for meth in string_methods:
             get1 = getattr(str(self.span),meth)('test')
             get2 = getattr(string,meth)('test')
             self.assertTrue(get1==get2)
         return None
    
    def test_contains(self,):
        """span1 in span2 verification"""
        text = 'I am a string used for a test with two different _a_ symbol'
        t1 = Span(text)
        t2 = Span(text,ranges=[range(5,6),])
        t3 = Span(text,ranges=[range(23,24),])
        t4 = Span('a')
        
        self.assertTrue(t2 in t1)
        self.assertFalse(t1 in t2)
        self.assertTrue(t3 in t1)
        self.assertFalse(t1 in t3)
        self.assertFalse(t2 in t3)
        self.assertTrue(str(t2) in t3)
        self.assertTrue(str(t3) in t2)
        self.assertFalse(t4 in t2)
        self.assertFalse(t4 in t3)
        self.assertFalse(t2 in t4)
        self.assertFalse(t3 in t4)
        self.assertTrue(str(t4) in t2)
        self.assertTrue(str(t4) in t3)
        return None
    
    def test_basicOperations(self,):
        """+ - * / verification"""
        text = 'I am a string following a conversation'
        t1 = Span(text)
        t2 = Span(text,ranges=[range(5,6),])
        t3 = Span(text,ranges=[range(24,25),])
        t4 = Span('a')
        
        t = t2+t3
        self.assertTrue(str(t)=='a a')
        self.assertTrue(t.ranges==[range(5, 6), range(24, 25)])
        self.assertTrue(t2.ranges==[range(5,6),])
        self.assertTrue(t3.ranges==[range(24,25),])
        
        t = t2-t3
        self.assertTrue(str(t)=='a')
        self.assertTrue(t==t2)
        self.assertTrue(t.ranges==[range(5,6),])
        self.assertTrue(t2.ranges==[range(5,6),])
        self.assertTrue(t3.ranges==[range(24,25),])
        
        t = t2-t1
        self.assertTrue(str(t)=='')
        self.assertFalse(bool(t))
        self.assertTrue(t.ranges==[])
        self.assertTrue(t2.ranges==[range(5,6),])
        self.assertTrue(t1.ranges==[range(len(text))])
        
        t = t1-t2
        self.assertTrue(str(t)=='I am   string following a conversation')
        self.assertTrue(t.ranges==[range(0, 5), range(6, 38)])
        self.assertTrue(t2.ranges==[range(5,6),])
        self.assertTrue(t1.ranges==[range(len(text))])
        
        with self.assertRaises(TypeError):
            t1-t4
        with self.assertRaises(TypeError):
            t1+t4
        with self.assertRaises(TypeError):
            t1*t4
        with self.assertRaises(TypeError):
            t1/t4
        
        t = t2/t3
        self.assertTrue(str(t)=='a a')
        self.assertTrue(t.ranges==[range(5, 6), range(24, 25)])
        self.assertTrue(t2.ranges==[range(5,6),])
        self.assertTrue(t3.ranges==[range(24,25),])
        
        t = t3/t2
        self.assertTrue(str(t)=='a a')
        self.assertTrue(t.ranges==[range(5, 6), range(24, 25)])
        self.assertTrue(t2.ranges==[range(5,6),])
        self.assertTrue(t3.ranges==[range(24,25),])
        
        t = t3/t3
        self.assertEqual(t.ranges, [])
        
        t = t2/t2
        self.assertEqual(t.ranges, [])
        
        t = t2*t3
        self.assertTrue(str(t)=='')
        self.assertFalse(bool(t))
        self.assertTrue(t.ranges==[])
        self.assertTrue(t2.ranges==[range(5,6),])
        self.assertTrue(t3.ranges==[range(24,25),])
        
        t2 = Span(text,ranges=[range(5,10),range(15,20)])
        t3 = Span(text,ranges=[range(8,12),])
        
        t = t2/t3
        self.assertTrue(str(t)=='a s in ollow')
        self.assertTrue(t.ranges==[range(5, 8), range(10, 12), range(15, 20)])
        self.assertTrue(t2.ranges==[range(5,10),range(15,20)])
        self.assertTrue(t3.ranges==[range(8,12),])
        
        t = t3/t2
        self.assertTrue(str(t)=='a s in ollow')
        self.assertTrue(t.ranges==[range(5, 8), range(10, 12), range(15, 20)])
        self.assertTrue(t2.ranges==[range(5,10),range(15,20)])
        self.assertTrue(t3.ranges==[range(8,12),])
        
        t = t2*t3
        self.assertTrue(str(t)=='tr')
        self.assertTrue(t.ranges==[range(8,10)])
        self.assertTrue(t2.ranges==[range(5,10),range(15,20)])
        self.assertTrue(t3.ranges==[range(8,12),])
        
        t = t2-t1
        self.assertFalse(bool(t))
        
        t = t1-t2
        self.assertTrue(bool(t))
        self.assertTrue(str(t)=='I am  ing f ing a conversation')
        self.assertTrue(t.ranges==[range(0, 5), range(10, 15), range(20, 38)])
        return None
 
    def test_add(self,):
        """Test the addition of two Span objects"""
        tok1 = Span(string=self.text,ranges=[range(10,20),])
        tok2 = Span(string=self.text,ranges=[range(30,40),])
        tok3 = tok1 + tok2
        s = self.text[10:20]+' '+self.text[30:40]
        self.assertTrue(s == str(tok3))
        tok1 += tok2
        self.assertTrue(s == str(tok1))
        self.assertTrue(tok1==tok3)
        return None
    
    def test_splitSimple(self,):
        """Test on Span.split for contiguous Span"""
        span = Span(string=self.text)
        splits = span.split([range(10,20),range(50,60)])
        strings = ['0123456789','1123456789','212345678931234567894123456789',
                   '5123456789','6123456789712345678981234567899123456789']
        ranges = [[range(0,10),],[range(10,20),],[range(20,50),],
                  [range(50,60),],[range(60,100),]]
        for sp,st,r in zip(splits,strings,ranges):
            sp_ = Span(string=self.text,
                       ranges=r)
            self.assertTrue(sp_==sp)
            self.assertTrue(str(sp)==st)
            self.assertTrue(sp.ranges==r)
        return None

    def test_splitDouble(self,):
        """Test on Span.split for double Span"""
        span = Span(string=self.text,
                    ranges=[range(10,20),range(50,90)])
        with self.assertRaises(IndexError):
            span.split([range(10,20),range(30,len(span)+2)])
        splits = span.split([range(10,20),range(30,40)])
        strings = ['1123456789',' 512345678','9612345678',
                   '9712345678','98123456789']
        ranges = [[range(10,20),],[range(20,20),range(50,59)],[range(59,69),],
                  [range(69,79),],[range(79,90),]]
        for sp,st,r in zip(splits,strings,ranges):
            sp_ = Span(string=self.text,
                       ranges=r)
            self.assertTrue(sp_==sp)
            self.assertTrue(str(sp)==st)
            self.assertTrue(sp.ranges==r)
        return None
    
    def test_slice(self,):
        """Test on Span.slice"""
        span = Span(string=self.text,
                    ranges=[range(10,20),range(30,40)])
        slices = span.slice(start=2,stop=13,size=2,step=1)
        strings = ['23','34','45','56','67','78','89','9 ',' 3','31']
        ranges = [[range(12,14),],[range(13,15),],[range(14,16),],[range(15,17),],
                  [range(16,18),],[range(17,19),],[range(18,20),],
                  [range(19,20),range(30,30)],[range(20,20),range(30,31)],[range(30,32),]]
        for sp,st,r in zip(slices,strings,ranges):
            sp_ = Span(string=self.text,
                       ranges=r)
            with self.subTest(line=sp):
                self.assertTrue(sp_==sp)
                self.assertTrue(str(sp)==st)
                self.assertTrue(sp.ranges==r)
        span = Span(string=self.text,
                    ranges=[range(10,20),range(30,40)])
        slices = span.slice(start=2,stop=15,size=3,step=3)
        strings = ['234','567','89 ','312',]
        ranges = [[range(12,15),],[range(15,18),],[range(18,20),range(30,30)],
                  [range(30,33),]]
        for sp,st,r in zip(slices,strings,ranges):
            sp_ = Span(string=self.text,
                       ranges=r)
            with self.subTest(line=sp):
                self.assertTrue(sp_==sp)
                self.assertTrue(str(sp)==st)
                self.assertTrue(sp.ranges==r)                
        return None
    
    def test_hash(self,):
        """Test the hash method of Span"""
        span = Span(string=self.text,
                    ranges=[range(10,20),range(30,40)])
        h1 = hash(span)
        span = Span(string=self.text,
                    ranges=[range(10,20),range(30,40)])
        h2 = hash(span)
        self.assertEqual(h1, h2)
        span = Span(string=self.text,
                    ranges=[range(10,20),])
        h_ = hash(span)
        self.assertNotEqual(h1, h_)
        return None
    
    def test_uniqueOrder(self,):
        """Test that there is only one ordering (>, <, >=, <=) True relation among two Span objects"""
        configurations = {}
        for start1 in range(5):
            for start2 in range(5):
                for size1 in range(1,4):
                    end1 = start1 + size1
                    for size2 in range(1,4):
                        end2 = start2 + size2
                        span1 = Span(self.text, ranges=[range(start1,end1),])
                        span2 = Span(self.text, ranges=[range(start2,end2),])
                        c1 = span1 < span2
                        c2 = span1 > span2
                        c3 = span1 <= span2
                        c4 = span1 >= span2
                        c5 = span1 == span2
                        configurations[(start1,end1,start2,end2)]=(c1,c2,c3,c4,c5)
        problems_, corrects_ = [], []
        for substitutions, bools in configurations.items():
            if sum(bools) != 1:      
                problems_.append((substitutions,bools))
            elif sum(bools) == 1:
                corrects_.append((substitutions,bools))
        self.assertEqual(len(problems_), 0)
        self.assertEqual(len(corrects_), 5*5*3*3)
        return None
        
if __name__ == '__main__':
    ut.main()
