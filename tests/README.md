# Tests

All the tests have been written using `unittest` module. Please launch all of them from a command line interface inside this folder as

```bash
python3 -m unittest -v
```

(verbosity `-v` is an option), and contact the authors (by issue raising on https://framagit.org/nlp/tokenspan) in case there are some failing test.