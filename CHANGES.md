# Change along the versions

Each main version comes with its own documentation, that is available as a [Jupyter-book](https://jupyterbook.org) that you can consult by opening `documentation/_build/html/index.html` on a web-browser. To generate the documentation, you need to extract the tagged versions on the [official repository](https://framagit.org/nlp/tokenspan), and to construct the corresponding documentation using `jupyter-book build documentation/`

## 0.5 - 

Versions prior to 0.5 were not released.

 - Versions before 0.4 only present the `Token` and `Tokens` classes. They have been splitted after in three classes, named `Span`, `Token` and `Tokens`. Importantly, the methods `Token.append` and `Token.remove` no longer exist in the next version. They have been replaced by `Token.append_range`, `Token.append_ranges`, `Token.remove_range` and `Token.remove_ranges`.
 - Version 0.4 add the class `Span` to `Token` and `Tokens`. `Span` handles the sub-parts splitting of a given string, whereas `Token` and `Tokens` now consumes `Span` objects and handle the attributes of the `Token`. 
 - From version 0.5, one has split the basic tools `Span`, `Token` and `Tokens` from the `iamtokenizing` package, see [pypi:iamtokenizing](https://pypi.org/project/iamtokenizing/). Only the advanced tokenizer are now present in the package `iamtokenizing`, which depends on the package `tokenspan`. The objects `Span`, `Token` and `Tokens` can be called as before from the newly deployed package `tokenspan`, available on https://pypi.org/project/tokenspan/.

## 0.5 + 

 - 0.5.1 : the `Span` objects are now hashable, such that one can use them in sets or dictionary keys
 - 0.5.2 : the `Span` objects can now be compared: `span1<span2`, `span1<=span2`, ...exist. This was an issue appearing with iamtokenizing package. Note the comparison makes sense only for contiguous `Span` (one-range-Span object)
 - 0.5.3 : correction of the symmetric_difference between similar `Span`s: `Span/Span` (or `Span.symmetric_division(Span)` ; the symmetric division of a `Span` by itself) now returns an empty Span object. Prior to this correction, the initialisation of a `Span` object constructed a complete range (i.e. a range covering the full string). See Issue #2.
 - 0.5.4 : `hash(Span)` now uses `hashlib.sha1` algorithm from standard library to avoid negative integer values and to allow string conversion. Due to the use of `bytes` conversion required by `hashlib`, the new class attribute `encoding` appeared, default to `utf-8`.
 - 0.5.5 : Span.union is corrected such that it stays empty in case one of the Span has empty ranges. Also Overclassing the Span class no more change the instance of the class while calling Span methods (Span(something) constructor has been changed to self.__class__(something) constructor).
 - 0.5.6 : the `Span` constructor now has None by default and construct the range for the entire string only in this case
 - 0.5.7 : the `Span` the order `span1<span2` for non-overlapping `Span`s and `span1<=span2` for overlapping `Span`s has been corrected to comply with thre reading order

## 0.6 +

 - 0.6.0 : main release, nothing more than 0.5.7, but the documentation has been updated online, and a version has been extracted on Pypi and as a release on the official repository, as [framagit.org/nlp/tokenspan:v0.6.0](https://framagit.org/nlp/tokenspan/-/tags/v0.6.0)
 - 0.6.1 : DeprecationWarnings for the method `get_subSpan(i)` and the property `subSpans`, that has been aliased by `get_subspan(i)` and `subspans`, respectively
 - 0.6.2 : Since the constructor has been modified in order to handle `ranges=None` and `ranges=list()`, there is no more need to recast an empty list of ranges in the algebraic operations. 
 - 0.6.3: the order functions have been rewritten, now the `Span` object can be ordered in 4 different ways for non-empty Span and non-equal Span, in addition if `span1 < span2` then `span2 > span1` and the same for `span1 <= span2` verify as well `span2 >= span1`. In addition, two `Span` are either in the `>`, `<`, `<=`, `>=` or `==` order relation for non-empty `Span`. So in principle there are only three order relation (`<`, `<=` and `==`, but `__eq__` is not designed as the third order relation in this version, and `>` is the dual of `<` while `>=` is the dual of `<=` (but `<=` is not the complement of `<`, though). It results that one can always construct an order relation among any non-equivalent (non-equal) Tokens constructed from the `Span`, and the implemented order is the reading order (from left to right). This might be important to generate several categories of Tokens on top of a given document: the ordering may classify the layers of Tokens.

## 0.7 + 
 - 0.7.0 : main release, nothing more than 0.6.3, but the documentation has been updated online, , and a version has been extracted on Pypi and as a release on the official repository, as [framagit.org/nlp/tokenspan:v0.7.0](https://framagit.org/nlp/tokenspan/-/tags/v0.7.0)