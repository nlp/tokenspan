{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Token and Tokens classes -  Chapter 2 : `ranges` and `subtoksep` attributes, and the Span representation\n",
    "\n",
    "In the previous chapter, we introduced the `Token` and `Tokens` objects as simple representation of a Python string. We showed the basic methods for splitting and re-gluing the different component of a string in term of its `Token` components, and how to collect them in `Tokens` instance. \n",
    "\n",
    "We here will show more about the passage from string to `Token`, and how to put several separated strings into the same `Token`, what is sometimes called a `Span` in other libraries. This process is handled directly by the `Token` object in the present construction. At some point it might be unclear, given the span extension of the `Token` class, why the `Tokens` class has been implemented. This will ultimately be clear in the next chapters : it is because one want to add attributes to the `Token`, and not to the `Tokens` ! \n",
    "\n",
    "There are several remarks in this NoteBook about the construction and limits of the design. They can be droped at first reading. In fact, most of the `ranges` and `subtoksep` properties are of no interest for basic usages of the `Token` and `Tokens` classes, so feel free to pass most of the materials covered in the present NoteBook and pass directly to the next chapter, where one implements a simple tokenizer in details.\n",
    "\n",
    "## Summary of the `ranges` and `subtoksep` attributes\n",
    "\n",
    "Every `Token` object has the following attributes : \n",
    " - `string` : the associated complete string\n",
    " - `ranges` : the intervals into the `string` which define the string representation of the `Token`, namely `str(Token)`. This attribute is a list of basic Python `range` objects.\n",
    " - `subtoksep` : when they are several `range` in the `ranges` attribute, the string representation `str(Token)` glues the different sub-tokens with `subtoksep` as the separator. To avoid missing some usefull behaviors, one advises to use a one-length string element as `subtoksep`, default being the space symbol `chr(32)` in Python terminology\n",
    "\n",
    "There are also `carry_attributes` and `parent` attributes to the `Token` class, but let us not think about that now.\n",
    "\n",
    "We start by instanciating a simple string, wich will serve as support for later illustrations of the `ranges` attribute.\n",
    "\n",
    "Recall that the `except ModuleNotFoundError` is here to handle the case where one has not installed the package."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [],
   "source": [
    "from tokenspan import Token, Tokens\n",
    "text = \"A really simple string for illustration.\""
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Basic Usage of `ranges` attribute\n",
    "\n",
    "Basic usage of the `Token` class is as a container for a string. It is constructed from a string, with argument `string` at the instanciation. More precisely, it has a sub-string behavior from the complete `Token.string` string. The way one pass from the complete string to its sub-string representation is through the `ranges` attribute. This attribute can be designed by hand, as we will do in the following for illustration. \n",
    "\n",
    "Recall that without `ranges` parameter when one instanciates the `Token` object, the `Token.ranges` attribute is designed to describe the entire `Token.string` string. \n",
    "\n",
    "Recall also that the `ranges` attribute must be a list of `range`, whatever might be the length of this list. In particular, for a single `range`, `Token.ranges=[range(start,stop)]` is the standard convention. Also, for all subsequent functionnalities, all `step` option of `range` **must be 1**. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[range(0, 40)]\n",
      "A really simple string for illustration.\n",
      "A really simple string for illustration.\n",
      "########################################\n",
      "[range(0, 8)]\n",
      "A really\n",
      "A really simple string for illustration.\n",
      "########################################\n",
      "[range(9, 22)]\n",
      "simple string\n",
      "A really simple string for illustration.\n",
      "########################################\n"
     ]
    }
   ],
   "source": [
    "token = Token(string=text)\n",
    "print(token.ranges)\n",
    "print(str(token))\n",
    "print(token.string)\n",
    "print(\"#\"*len(token.string))\n",
    "\n",
    "token.ranges = [range(8)]\n",
    "print(token.ranges)\n",
    "print(str(token))\n",
    "print(token.string)\n",
    "print(\"#\"*len(token.string))\n",
    "\n",
    "token.ranges = [range(9,22)]\n",
    "print(token.ranges)\n",
    "print(str(token))\n",
    "print(token.string)\n",
    "print(\"#\"*len(token.string))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now, let us construct a few `Token` from the same string."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "A really\n",
      "simple string\n",
      "illustration\n",
      "A simple string illustration\n"
     ]
    }
   ],
   "source": [
    "tok1 = Token(string=text,ranges=[range(8)])\n",
    "print(str(tok1))\n",
    "tok2 = Token(string=text,ranges=[range(9,22)])\n",
    "print(str(tok2))\n",
    "tok3 = Token(string=text,ranges=[range(27,39)])\n",
    "print(str(tok3))\n",
    "tok4 = Token(string=text,ranges=[range(0,1),range(9,22),range(27,39)])\n",
    "print(str(tok4))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "One sees that there is not much differences between a string representation of a `Token` object having one or several `range`. This is in fact where the `Token.subtoksep` appears. Let us change this parameter for the above examples. By default this parameter is the space symbol."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "A really\n",
      "simple string\n",
      "illustration\n",
      "A_simple string_illustration\n"
     ]
    }
   ],
   "source": [
    "tok1 = Token(string=text,\n",
    "             ranges=[range(8)],\n",
    "             subtoksep='_')\n",
    "print(str(tok1))\n",
    "tok2 = Token(string=text,\n",
    "             ranges=[range(9,22)],\n",
    "             subtoksep='_')\n",
    "print(str(tok2))\n",
    "tok3 = Token(string=text,\n",
    "             ranges=[range(27,39)],\n",
    "             subtoksep='_')\n",
    "print(str(tok3))\n",
    "tok4 = Token(string=text,\n",
    "             ranges=[range(0,1),range(9,22),range(27,39)],\n",
    "             subtoksep='_')\n",
    "print(str(tok4))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "If there is no change when there is a single `range`, one sees that calling `str(Token)` when there are several `range` will automatically glue the different sub-tokens using the `subtoksep`. But the usual spaces (as any other character in fact) inside a given `range` is not affected by the `subtoksep`, see `'simple string'` in `tok4`.\n",
    "\n",
    "## Slicing procedure in a `Token`\n",
    "\n",
    "How is the slicing process handled in a `Token` ? Well, the `subtoksep` counts as any other other character in the string representation. It also counts in `len` in fact. Let us illustrate this."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "A rea\n",
      "A_sim\n",
      "ple s\n",
      "tring_illu\n"
     ]
    }
   ],
   "source": [
    "print(tok1[:5])\n",
    "print(tok4[:5])\n",
    "print(tok4[5:10])\n",
    "print(tok4[10:20])"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "28\n",
      "5\n"
     ]
    }
   ],
   "source": [
    "print(len(tok4))\n",
    "print(len(tok4[:5]))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "And if we change the `subtoksep`, its length is automatically taken into account for the calculation of the length of the complete `Token` and the slicing process."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "A_&_simple string_&_illustration\n",
      "32\n",
      "A_&_s\n"
     ]
    }
   ],
   "source": [
    "tok5 = Token(string=text,\n",
    "             ranges=[range(0,1),range(9,22),range(27,39)],\n",
    "             subtoksep='_&_')\n",
    "print(str(tok5))\n",
    "print(len(tok5))\n",
    "print(tok5[:5])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "So the `Token` class handles all the machinery of a quite normal string, thanks to the `ranges` and `subtoksep` attributes.\n",
    "\n",
    "## Absolute and relative coordinates\n",
    "\n",
    "It seems everything is handled underneath such that a `Token` object is just a sub-string of its `Token.string` string. But there are sometimes some subtleties to be understood. one of them is the absolute versus relative coordinates, which may give headaches to users. Fortunately enough, things are quite workable, trusting the machinery behind the `Token` class. \n",
    "\n",
    "The _absolute coordinate_ system is the position of the text inside the parent string given at the instanciation of the `Token` object. Most of the time, one should not worry about it, except perhaps in the `Token.append` and `Token.remove` methods that will be discussed later in this chapter.\n",
    "\n",
    "The _relative coordinate_ system is the position inside the `Token.ranges` object. This is the natural position if one sees the `Token` as its string representation, namely `str(Token)`.\n",
    "\n",
    "So in short : \n",
    " - absolute position refers to the position in the string `Token.string`\n",
    " - relative coordinate refers to the position in the string `str(Token)`\n",
    "\n",
    "As an example, let us construct a simple string of digits, where the digit `0` appears at position `0`, the digit `1` at position `10`, the digit `2` at position `20` and so on. In between of decade, there are the natural digits from `1` to `9`. On top of this string, we construct the `Token` which will represent a string of size `40` made of one decade over two, up to position `80` of the `digits` string."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "'1123456789#3123456789#5123456789#7123456789'"
      ]
     },
     "execution_count": 8,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "root = '123456789'\n",
    "digits = ''.join([str(i)+root for i in range(10)])\n",
    "tok_digits = Token(string=digits,\n",
    "                   ranges=[range(10,20),range(30,40),\n",
    "                           range(50,60),range(70,80)],\n",
    "                  subtoksep='#')\n",
    "str(tok_digits)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Then the relative coordinates range from `0` to `39+3*len(subtoksep)` since there are 4 ranges and so 3 `subtoksep` separators inserted, whereas the absolute ones range from `0` to `99` without interruption of a `subtoksep`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Relative coordinates from 0 to 20\n",
      "1123456789#312345678\n",
      "Relative length: 43\n",
      "\n",
      "\n",
      "Absolute coordinates from 0 to 20\n",
      "01234567891123456789\n",
      "Absolute length: 100\n"
     ]
    }
   ],
   "source": [
    "print(\"Relative coordinates from 0 to 20\")\n",
    "print(str(tok_digits)[:20])\n",
    "print(\"Relative length: {}\".format(len(tok_digits)))\n",
    "print(\"\\n\")\n",
    "print(\"Absolute coordinates from 0 to 20\")\n",
    "print(tok_digits.string[:20])\n",
    "print(\"Absolute length: {}\".format(len(tok_digits.string)))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "One more time, this is just a quite ridiculous complexity in the presentation for almost nothing, since most of the usages will never find un-natural outcome using the basic tools of `Token` and `Tokens` classes, as long as `subtoksep` is of length 1. \n",
    "\n",
    "## Enlarge the Token, and combine overlapping ranges\n",
    "\n",
    "Suppose one want to collapse, for some reason, `tok1` and `tok2` in a new `Token` called `tok12`. Then the related string representation will be given by the concatenation of the two previous strings, and the resulting `ranges` attribute illustrate the combination, as well as the `subtoksep` that is now present."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 10,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "A really_simple string\n",
      "[range(0, 8), range(9, 22)]\n"
     ]
    }
   ],
   "source": [
    "tok12 = tok1 + tok2\n",
    "print(tok12) # equivalent to print(str(tok12))\n",
    "print(tok12.ranges)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Note that this `Token` has not mush difference with the initial string from position 0 to 22, except for the `subtoksep` that is different from a normal space symbol in our illustration.\n",
    "\n",
    "Note in passing that there are blocking process that avoid adding two `Token` if they do not have the same `subtoksep`..."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 11,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "Token('A really_simple string_illustration', [(0,8),(9,22),(27,39)])"
      ]
     },
     "execution_count": 11,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "tok1 + tok5"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    ".. and one can compare the resulting `Token`, in this case it is `tok1`. In that case the addition is not commutative : `tok1 + tok5 != tok5 + tok1` ! (comparing `Token` will be detailled in a later chapter)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 12,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "False\n",
      "False\n",
      "False\n"
     ]
    }
   ],
   "source": [
    "print(tok1 + tok5 == tok1)\n",
    "print(tok5 + tok1 == tok5)\n",
    "print(tok1 == tok5)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In practise, the second `Token` is simply rejected from the concatenation construction.\n",
    "\n",
    "Let us come back to the concatenation procedure. What would happen if one try to concatenate `tok3` and `tok4`, since they have a part of the initial string in common ? In fact they are special handling underneath, which will recalculate all `ranges` such that overlapping disapear."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 13,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "A_simple string_illustration\n",
      "[range(0, 1), range(9, 22), range(27, 39)]\n",
      "True\n"
     ]
    }
   ],
   "source": [
    "tok34 = tok3 + tok4\n",
    "print(tok34)\n",
    "print(tok34.ranges)\n",
    "print(tok34 == tok4)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Note that the two different processes are not the same at all ! `tok34 == tok3` because the string representation of `tok4` is already present in `tok3`, whereas `tok1` and `tok5` were simply incompatible for addition ! This can be seen because the addition in this later case is commutative, as illustrated by the last line below."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 14,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "A_simple string_illustration\n",
      "[range(0, 1), range(9, 22), range(27, 39)]\n",
      "True\n",
      "True\n"
     ]
    }
   ],
   "source": [
    "tok43 = tok4 + tok3\n",
    "print(tok43)\n",
    "print(tok43.ranges)\n",
    "print(tok43 == tok4)\n",
    "print(tok34 == tok43)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "To illustrate further the non-overlapping catching, let us try to create a new `Token` with some overlapping `range` objects, and realise that the construction in fact destroys the independant `range` and fuse them towards a single-`range` `Token` instance."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 15,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "A really simple string_illustration\n",
      "[range(0, 22), range(27, 39)]\n"
     ]
    }
   ],
   "source": [
    "tok6 = Token(string=text,\n",
    "             ranges=[range(0,9),range(9,22),range(27,39),range(30,39),range(10,15)],\n",
    "             subtoksep='_')\n",
    "print(tok6)\n",
    "print(tok6.ranges)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "There is only two `range` surviving the construction process, since `range(10,15)` is entirely contained in `range(9,22)`, and the same is true for `range(30,39)` which give no more information than `range(27,39)` relative to the initial string. In addition the two ranges `range(0,9)` and `range(9,22)` are transformed naturally to the `range(0,22)` since they in fact represent this entire range when taking together.\n",
    "\n",
    "### The combining function (for developpers)\n",
    "\n",
    "For those interested in the construction of the `Token` class, we reproduce the function `_combineRanges(ranges)` which avoids the proliferation of overlapping `range` in the `Token`. This function is available in the `tokenizer/tokentokens.py` module."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 16,
   "metadata": {},
   "outputs": [],
   "source": [
    "def _combineRanges(ranges):\n",
    "    \"\"\"\n",
    "Take a list of range objects, and transform it such that overlapping \n",
    "ranges and consecutive ranges are combined. \n",
    "\n",
    "`ranges` is a list of `range` object, all with `range.step==1` \n",
    "(not verified by this function, but required for \n",
    "the algorithm to work properly).\n",
    "    \"\"\"\n",
    "    if len(ranges)<2:\n",
    "        return ranges\n",
    "    r_ = sorted([(r.start,r.stop) for r in ranges])\n",
    "    temp = [list(r_[0]),]\n",
    "    for start,stop in r_[1:]:\n",
    "        if temp[-1][1] >= start:\n",
    "            temp[-1][1] = max(temp[-1][1], stop)\n",
    "        else:\n",
    "            temp.append([start, stop])\n",
    "    r = [range(*t) for t in temp]\n",
    "    return r"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## `Token.append` and `Token.remove`\n",
    "\n",
    "There are two facilities to design the `Token.ranges` attributes, namely the one which add a `range` to `Token.ranges` using a method called `Token.append(range)` or `Token.append(list_of_range)`, and a method to remove a `range`, called `Token.remove(range)` or `Token.remove(list_of_range)`. \n",
    "\n",
    "Note that appending a `range` using `Token.append` also check for overlapping interval, and will not duplicate the `range` in `Token.ranges`. Since one want to append a new `range`, this range is given in absolute coordinates, that is, in the counting of `Token.string`.\n",
    "\n",
    "In the contrary, `Token.remove` will withdraw the passing `range` from the relative coordinates. That is, if the removed range is not overlapping with some `Token.ranges`, it will not be removed. Nevertheless, `Token.remove` uses the absolute coordinates.\n",
    "\n",
    "If you are not at ease with the names `append` and `remove`, because they are too close to the Python list methods, you can use `append_range` and `remove_range`, which are aliases for the two previous ones.\n",
    "\n",
    "Importantly, `append`, `remove` and their aliases work _in place_, i.e. they transform the `Token` object itself. This is the same behavior as the same methods of a Python list.\n",
    "\n",
    "To illustrate this, we come back to our digits string introduced in [the discussion about absolute and relative coordinates](#Absolute-and-relative-coordinates) above. The `tok_digits` instance has `ranges` attributes in the form `[(10,20),(30,40),(50,60),(70,80)]`, so appending the range `(20,30)` should fuse its two first sub-ranges because the overlapping are automatically fusionned after an `Token.append` process. From the resulting `Token.append` one can remove the same range `(20,30)` to come back to the intial object. If one remove the range `(15,35)`, then one should end up with a `Token.ranges` of the form `[(10,25),(35,40),(50,60),(70,80)]`. Let us see all of this (plus a few more) in the examples below."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 17,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[range(10, 20), range(30, 40), range(50, 60), range(70, 80)]\n",
      "append (20,30)\n",
      "[range(10, 40), range(50, 60), range(70, 80)]\n",
      "remove (20,30)\n",
      "[range(10, 20), range(30, 40), range(50, 60), range(70, 80)]\n",
      "remove (15,35)\n",
      "[range(10, 15), range(35, 40), range(50, 60), range(70, 80)]\n",
      "remove (15,35) and (75,125), far too long for the Token.string\n",
      "[range(10, 15), range(35, 40), range(50, 60), range(70, 75)]\n"
     ]
    }
   ],
   "source": [
    "print(tok_digits.ranges)\n",
    "print(\"append (20,30)\")\n",
    "tok_digits.append_range(range(20,30))\n",
    "print(tok_digits.ranges)\n",
    "print(\"remove (20,30)\")\n",
    "tok_digits.remove_range(range(20,30))\n",
    "print(tok_digits.ranges)\n",
    "print(\"remove (15,35)\")\n",
    "tok_digits.remove_range(range(15,35))\n",
    "print(tok_digits.ranges)\n",
    "print(\"remove (15,35) and (75,125), far too long for the Token.string\")\n",
    "tok_digits.remove_ranges([range(15,35),range(75,125)])\n",
    "print(tok_digits.ranges)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "One sees that removing a range that does not exist in the absolute coordinates produce nothing (that's the example of removing the range `(75,125)` at the last step, which in fact remove only the range `(75,80)` as this is the only available one in the `Token` object at that step).\n",
    "\n",
    "In the same way, adding a range from the outside of the `Token.string` will produce nothing."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 18,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[range(10, 15), range(35, 40), range(50, 60), range(70, 75)]\n",
      "append (120,130)\n",
      "[range(10, 15), range(35, 40), range(50, 60), range(70, 75)]\n"
     ]
    },
    {
     "name": "stderr",
     "output_type": "stream",
     "text": [
      "/home/gozat/.local/lib/python3.8/site-packages/tokenspan/tools.py:28: BoundaryWarning: At least one of the boundaries has been modified\n",
      "  warnings.warn(mess, category=BoundaryWarning)\n"
     ]
    }
   ],
   "source": [
    "print(tok_digits.ranges)\n",
    "print(\"append (120,130)\")\n",
    "tok_digits.append_range(range(120,130))\n",
    "print(tok_digits.ranges)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now we pass to the basic explanation of how the `Token.ranges` transform towards the `Tokens` objets.\n",
    "\n",
    "## From `Token` to `Tokens` classes\n",
    "\n",
    "The three mechanisms to pass from `Token` to `Tokens` are using either the `partition`, `split` or `slice` methods. We review they mechanisms once a multi-ranged `Token` is involved in the process. \n",
    "\n",
    "One more time, we prefer the debugging representation of the `Tokens` class for illustration, or its list representation, than the `str(Tokens)` representation, considered more messy.\n",
    "\n",
    "We first see that everything is done to not bother the user with the position arguments, the `start` and `stop` parameter of `Token.partition` is calculated from the string representation `str(Token)`. In addition, the `subtoksep` are conserved by the splitting processes. So, when one cuts the `tok4` string from position `start=2` to position `stop=8` using the `partition` method, one really isolates `str(tok4)[2:8]` in the middle `Token` of the resulting `Tokens`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 19,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[Token('A_', [(0,1),(9,9)]), Token('simple', [(9,15)]), Token(' string_illustration', [(15,22),(27,39)])]\n",
      "True\n",
      "True\n",
      "True\n"
     ]
    }
   ],
   "source": [
    "tokens = tok4.partition(2,8)\n",
    "print(list(tokens))\n",
    "print(str(tokens[0])==str(tok4)[:2])\n",
    "print(str(tokens[1])==str(tok4)[2:8])\n",
    "print(str(tokens[2])==str(tok4)[8:])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The way the `subtoksep` are conserved is due to the insertion of empty `range` in the `Token` combined in the `Tokens` object : see the first line below."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 20,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[range(0, 1), range(9, 9)]\n",
      "[range(9, 15)]\n",
      "[range(15, 22), range(27, 39)]\n"
     ]
    }
   ],
   "source": [
    "for tok in tokens:\n",
    "    print(tok.ranges)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Note finally that the behavior is less clear as soon as one use a `subtoksep` with length larger than 1, as is illustrated below, where several different solutions exist for the same strings. This is because it is quite clear in Python what to do with a `range(start,stop)` which always corresponds to a semi-open (mathematical) interval `[start,stop[` including the `start` and rejecting the `stop`, but when either the `start` or the `stop` pops on a `subtoksep`, does one have to put it in the left or in the right interval after splitting the string ? The answer is below : if you design the splitting to be performed for a one-character sized `subtoksep`, the algorithm wait for the `subtoksep` to be entirely on the left of `stop` to display it in the left `range`. See the illustration below.\n",
    "\n",
    "**This is the reason why using `len(subtoksep)==1` is highly recommended.**"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 21,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[Token('A', [(0,1)]), Token('_&_simp', [(1,1),(9,13)]), Token('le string_&_illustration', [(13,22),(27,39)])]\n",
      "total length = 32\n",
      "[Token('A', [(0,1)]), Token('simple', [(9,15)]), Token(' string_&_illustration', [(15,22),(27,39)])]\n",
      "total length = 29\n",
      "[Token('A', [(0,1)]), Token('simple', [(9,15)]), Token(' string_&_illustration', [(15,22),(27,39)])]\n",
      "total length = 29\n",
      "[Token('A_&_', [(0,1),(9,9)]), Token('simple', [(9,15)]), Token(' string_&_illustration', [(15,22),(27,39)])]\n",
      "total length = 32\n"
     ]
    }
   ],
   "source": [
    "tokens = tok5.partition(1,8)\n",
    "print(list(tokens))\n",
    "print(\"total length = {}\".format(sum(len(tok) for tok in tokens)))\n",
    "tokens = tok5.partition(2,8)\n",
    "print(list(tokens))\n",
    "print(\"total length = {}\".format(sum(len(tok) for tok in tokens)))\n",
    "tokens = tok5.partition(3,9)\n",
    "print(list(tokens))\n",
    "print(\"total length = {}\".format(sum(len(tok) for tok in tokens)))\n",
    "tokens = tok5.partition(4,10)\n",
    "print(list(tokens))\n",
    "print(\"total length = {}\".format(sum(len(tok) for tok in tokens)))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In contrary, using a correct `subtoksep` with length 1 never destroys the `Token` string."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 22,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[Token('A', [(0,1)]), Token('_simple', [(1,1),(9,15)]), Token(' string_illustration', [(15,22),(27,39)])]\n",
      "total length = 28\n",
      "[Token('A_', [(0,1),(9,9)]), Token('simple', [(9,15)]), Token(' string_illustration', [(15,22),(27,39)])]\n",
      "total length = 28\n"
     ]
    }
   ],
   "source": [
    "tokens = tok4.partition(1,8)\n",
    "print(list(tokens))\n",
    "print(\"total length = {}\".format(sum(len(tok) for tok in tokens)))\n",
    "tokens = tok4.partition(2,8)\n",
    "print(list(tokens))\n",
    "print(\"total length = {}\".format(sum(len(tok) for tok in tokens)))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "An other important special case is when one tries to split the initial `Token` on either its initial or final character. See the example below"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 23,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[Token('', [(0,0)]), Token('A', [(0,1)]), Token('_simple string_illustration', [(1,1),(9,22),(27,39)])]\n",
      "[Token('A_simple string_', [(0,1),(9,22),(27,27)]), Token('illustration', [(27,39)]), Token('', [(39,39)])]\n"
     ]
    }
   ],
   "source": [
    "tokens = tok4.partition(0,1)\n",
    "print(list(tokens))\n",
    "tokens = tok4.partition(len(tok4)-12,len(tok4))\n",
    "print(list(tokens))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In that case, the left-most of right-most `Token` is empty. Nevertheless, one can remedy to that by using the parameter `remove_empty=True` (default is `False`) when calling `partition`. Note is kills one `Token` in that case."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 24,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[Token('A', [(0,1)]), Token('_simple string_illustration', [(1,1),(9,22),(27,39)])]\n",
      "[Token('A_simple string_', [(0,1),(9,22),(27,27)]), Token('illustration', [(27,39)])]\n"
     ]
    }
   ],
   "source": [
    "tokens = tok4.partition(0,1,remove_empty=True)\n",
    "print(list(tokens))\n",
    "tokens = tok4.partition(len(tok4)-12,len(tok4),True)\n",
    "print(list(tokens))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The behaviors of `split` and `slice` are quite similar, so we simply give one example of each"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 25,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[Token('', [(0,0)]), Token('A', [(0,1)]), Token('_', [(1,1),(9,9)]), Token('simple', [(9,15)]), Token(' string_', [(15,22),(27,27)]), Token('illustration', [(27,39)]), Token('', [(39,39)])]\n",
      "[Token('A', [(0,1)]), Token('_', [(1,1),(9,9)]), Token('simple', [(9,15)]), Token(' string_', [(15,22),(27,27)]), Token('illustration', [(27,39)])]\n"
     ]
    }
   ],
   "source": [
    "tokens = tok4.split((range(0,1),range(2,8),\n",
    "                     range(len(tok4)-12,len(tok4))),remove_empty=False)\n",
    "print(list(tokens))\n",
    "tokens = tok4.split((range(0,1),range(2,8),\n",
    "                     range(len(tok4)-12,len(tok4))),remove_empty=True)\n",
    "print(list(tokens))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Remark how the `Token` containing only a `subtoksep` handles its length: by keeping two empty `range`, which ensure its length is still the one of the `subtoksep`. One more time this can not be done using `len(subtoksep)>1`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 26,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "[range(1, 1), range(9, 9)]"
      ]
     },
     "execution_count": 26,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "tokens = tok4.split((range(0,1),range(2,8),\n",
    "                     range(len(tok4)-12,len(tok4))),remove_empty=True)\n",
    "tokens[1].ranges"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## From `Tokens` to `Token` classes\n",
    "\n",
    "Let us now study the `join` procedure. Basically, `Tokens.join(start,stop,step)` will take all `Token` that are in the slice `Tokens[start:stop:step]` and concatenates them.\n",
    "Recall that mising argument are calculate to fill the `Tokens`, but order must be conserved, or one must call them explicitely using their names when calling the method."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 27,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[Token('A', [(0,1)]), Token('_', [(1,1),(9,9)]), Token('simple', [(9,15)]), Token(' string_', [(15,22),(27,27)]), Token('illustration', [(27,39)])]\n"
     ]
    }
   ],
   "source": [
    "print(list(tokens))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 28,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "A_simple string_illustration\n",
      "[range(0, 1), range(9, 22), range(27, 39)]\n"
     ]
    }
   ],
   "source": [
    "tok = tokens.join()\n",
    "print(tok)\n",
    "print(tok.ranges)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 29,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "_simple string_\n",
      "[range(1, 1), range(9, 22), range(27, 27)]\n"
     ]
    }
   ],
   "source": [
    "tok = tokens.join(1,4,1)\n",
    "print(tok)\n",
    "print(tok.ranges)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 30,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "simple_illustration\n",
      "[range(9, 15), range(27, 39)]\n"
     ]
    }
   ],
   "source": [
    "tok = tokens.join(start=2,step=2)\n",
    "print(tok)\n",
    "print(tok.ranges)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Underneath, the association use only the `ranges`, picking the elements `[start:stop:step]` from the below list, then applying the `_combineRanges` to the resulting elements, and reconstructing a `Token` object from those."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 31,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[[range(0, 1)], [range(1, 1), range(9, 9)], [range(9, 15)], [range(15, 22), range(27, 27)], [range(27, 39)]]\n"
     ]
    }
   ],
   "source": [
    "print([tok.ranges for tok in tokens])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "As a final remark, let us realize that all the `Token` generated in this NoteBook nonetheless have the same string, but this string is not conserved by any of them, since objects are passed by reference in Python. \n",
    "\n",
    "So keeping the complete string in a great number of `Token` objects will not bring any memory usage trouble in principle."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 32,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "True\n",
      "True\n"
     ]
    }
   ],
   "source": [
    "strings = [tok.string for tok in [tok1,tok2,tok3,tok4,tok5]]\n",
    "bools = [strings[0]==s for s in strings[1:]]\n",
    "print(all(bools))\n",
    "stringsId = [id(tok.string) for tok in [tok1,tok2,tok3,tok4,tok5]]\n",
    "bools = [stringsId[0]==s for s in stringsId[1:]]\n",
    "print(all(bools))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 33,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Last modification Wed Jan 19 19:56:03 2022\n"
     ]
    }
   ],
   "source": [
    "from datetime import datetime\n",
    "print(\"Last modification {}\".format(datetime.now().strftime(\"%c\")))"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.10"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}